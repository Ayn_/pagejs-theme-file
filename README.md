# PageJS Themes

PageJS themes are just a JSON file that defines custom style and script tags as well as sudo tags and classes.

## Syntax

```json
[   
    {
        "name": "Template",
        "link": {
            "attributes": {
                "rel": "stylesheet",
                "href":"cdn url",
                "integrity": "",
                "crossorigin":""
            }
        },
        "script": [
            { 
                "attributes" : {
                    "src":"cdn url",
                    "integrity": "",
                    "crossorigin":""
                }
            }
        ]
    },
    {
        "container": {
            "div": {
                "attributes": {
                    "class": {
                        "default": "container",
                        "fluid": "container-fluid"                    
                    }
                }
            }
        }
    }
]
```

The first object in the array defines links and scripts required by this theme.

The second object in the array allows you to define your custom tags. 
We can consume it though PageJS like so:
```js
new Page()
    .container({ class: 'fluid' })
```
If we chose not to define a class for our sudo tag it would default to the first object. If you don't wish to 
define sudo classes for your sudo tag you can pass a string instead of an object. Ex: `class: 'container'`.